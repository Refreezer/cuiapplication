﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Windows.Forms.DataVisualization.Charting;

namespace CUIApplication
{
    public partial class MainForm : Form
    {
        private string _fileName = null;
        private readonly Dictionary<string, long> _statistics = new Dictionary<string, long>();
        private string _currentStatisticsName;

        public MainForm()
        {
            InitializeComponent();
        }


        /**
         * Метод срабатывает при нажатии на File/Open
         * Вызывается диалоговое окно файла
         * При неверных данных в файле вызывается окно с сообщением об ошибке
         * Иначе - данные считывается и строится диаграмма
         */
        private void OpenTab_Click(object sender, EventArgs e)
        {
            switch (openFileDialog.ShowDialog())
            {
                case (DialogResult.OK):
                    _fileName = openFileDialog.FileName;
                    try
                    {
                        ParseFile();
                    }
                    catch (Exception exception)
                    {
                        MessageBox.Show("Input file contains invalid data:\n" + exception.Message, 
                            "Error",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                        break;
                    }

                    FillChart();


                    break;
                case (DialogResult.Cancel):
                case (DialogResult.Abort):
                case (DialogResult.None):
                    break;
                default:
                    //TODO--обработать остальные случаи закрытия окна выбора файла
                    MessageBox.Show("Smthng went wrong",
                        "Error",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    break;
            }
        }

        private void AboutTab_Click(object sender, EventArgs e)
        {
            //TODO--создать файл, содержащий текстовую информацию выводимую по нажатию кнопки about

            const string infoFileName = "";
            // const string infoText = File.ReadAllText(infoFileName);
            MessageBox.Show("Info",
                "Info",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        }

        private void QuitTab_Click(object sender, EventArgs e)
        {
            this.Close();
            if (Application.AllowQuit)
            {
                Application.Exit();
            }
        }


        #region Helpers

        /**
         * Считывает файл,
         * инициализирует приватные поля
         */
        private void ParseFile()
        {
            IList<string> lines = File.ReadAllLines(_fileName);
            _currentStatisticsName = lines[0];
            foreach (var line in lines.Skip(1))
            {
                var valueKey = line.Split(new[] {' '}, 2);
                if (valueKey.Length != 2) throw new Exception("Invalid data in line : " + line);
                _statistics.Add(valueKey[1], long.Parse(valueKey[0]));
            }
        }

        /**
         * Заполняет диаграмму
         * 
         */
        private void FillChart()
        {
            statisticsChart.Titles.Clear();
            statisticsChart.Titles.Add(_currentStatisticsName);


            statisticsChart.Series.Clear();
            statisticsChart.Series.Add("Votes");
            foreach (var statistic in _statistics)
            {
                statisticsChart.Series["Votes"].Points.AddXY(statistic.Key, statistic.Value);
            }

            statisticsChart.Palette = ChartColorPalette.Berry;
            if (!statisticsChart.Visible)
            {
                statisticsChart.Visible = true;
            }
        }

        #endregion
    }
}